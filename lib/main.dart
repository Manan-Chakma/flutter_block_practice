import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_state.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_state.dart';
import 'package:flutter_block_communication/constants/enums.dart';
import 'package:flutter_block_communication/presentation/pages/home.dart';
import 'package:flutter_block_communication/presentation/router/app_router.dart';

void main() {
  runApp(MyApp(Connectivity()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Connectivity connectivity;

  MyApp(this.connectivity);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<InternetCubit>(
            create: (context) => InternetCubit(connectivity),
          ),
          BlocProvider<CounterCubit>(
            create: (context) => CounterCubit(context.read<InternetCubit>()),
          )
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: '/',
          onGenerateRoute: AppRouter.onGenerateRoute,
        )
    );
  }
}
