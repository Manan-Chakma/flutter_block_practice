import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_state.dart';
import 'package:flutter_block_communication/constants/enums.dart';

class InternetCubit extends Cubit<InternetState> {
  final Connectivity connectivity;
  StreamSubscription subscription;

  InternetCubit(this.connectivity) : super(InternetLoading()){
    subscription = connectivity.onConnectivityChanged.listen((connectivity) {
      if(connectivity == ConnectivityResult.wifi) {
        internetConnected(ConnectionType.WIFI);
      } else if(connectivity == ConnectivityResult.mobile) {
        internetConnected(ConnectionType.MOBILE);
      } else if(connectivity == ConnectivityResult.none){
        internetDisconnected();
      }
    });
  }


  void internetConnected(ConnectionType connectionType) => emit(InternetConnected(connectionType));
  void internetDisconnected() => emit(InternetDisconnected());

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }

}