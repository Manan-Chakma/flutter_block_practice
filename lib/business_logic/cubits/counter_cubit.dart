import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_state.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_state.dart';
import 'package:flutter_block_communication/constants/enums.dart';

class CounterCubit extends Cubit<CounterState> {
  final InternetCubit internetCubit;
  StreamSubscription internetSubscription;

  CounterCubit(this.internetCubit) : super(CounterState(0)){
    internetSubscription = internetCubit.listen((internetState) {
      if(internetState is InternetConnected && internetState.connectionType == ConnectionType.WIFI) {
        increment();
      } else if(internetState is InternetConnected && internetState.connectionType == ConnectionType.MOBILE) {
        decrement();
      }
    });
  }

  void increment() => emit(CounterState(state.counter + 1));
  void decrement() => emit(CounterState(state.counter - 1));

  @override
  Future<void> close() {
    internetSubscription.cancel();
    return super.close();
  }
}