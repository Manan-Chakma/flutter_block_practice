import 'package:flutter_block_communication/constants/enums.dart';

abstract class InternetState{}



class  InternetLoading extends InternetState{}

class InternetConnected extends InternetState{

  final ConnectionType connectionType;

  InternetConnected(this.connectionType);
}

class InternetDisconnected extends InternetState{}