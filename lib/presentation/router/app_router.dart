import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_block_communication/presentation/pages/dashboard.dart';
import 'package:flutter_block_communication/presentation/pages/home.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => MyHomePage(title: 'Flutter Demo Home Page'));
      case '/dashboard':
        return MaterialPageRoute(builder: (context) => Dashboard());
      default:
        return _errorRoute();
    }
  }
  static Route<dynamic> _errorRoute(){
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
      );
    });
  }

}
