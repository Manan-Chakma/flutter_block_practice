import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_state.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_state.dart';
import 'package:flutter_block_communication/constants/enums.dart';
import 'package:flutter_block_communication/presentation/pages/dashboard.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BlocBuilder<InternetCubit, InternetState>(
                builder: (context, state) {
              if (state is InternetConnected &&
                  state.connectionType == ConnectionType.WIFI) {
                return Text('wifi');
              } else if (state is InternetConnected &&
                  state.connectionType == ConnectionType.MOBILE) {
                return Text('mobile');
              } else if (state is InternetDisconnected) {
                return Text('disconnected');
              }
              return Text('nothing');
            }),
            BlocConsumer<CounterCubit, CounterState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state.counter < 0) {
                    return Text(
                        state.counter.toString() + ' is a negative number');
                  } else if (state.counter % 2 == 0) {
                    return Text(state.counter.toString() + ' is even number');
                  }
                  return Text(state.counter.toString());
                }),
            MaterialButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/dashboard');
              },
              child: Text('Go to second activity'),
            )
          ],
        ),
      )
    );
  }
}
