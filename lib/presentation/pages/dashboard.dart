import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_state.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Center(
        child: Container(
          child: Column(
            children: [
              BlocConsumer<CounterCubit, CounterState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state.counter < 0) {
                      return Text(
                          state.counter.toString() + ' is a negative number');
                    } else if (state.counter % 2 == 0) {
                      return Text(state.counter.toString() + ' is even number');
                    }
                    return Text(state.counter.toString());
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
