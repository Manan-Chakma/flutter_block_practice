import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_cubit.dart';
import 'package:flutter_block_communication/business_logic/cubits/counter_state.dart';
import 'package:flutter_block_communication/business_logic/cubits/internet_cubit.dart';
import 'package:test/test.dart';

void main() {
  group('CounterCubit', () {
    CounterCubit counterCubit;
    InternetCubit internetCubit = InternetCubit(Connectivity());
    setUp(() {
      counterCubit = CounterCubit(internetCubit);
    });
    tearDown(() {
      counterCubit.close();
    });

    test('description', () {
      expect(counterCubit.state, CounterState(0));
    });
  });
}
